from Bio.Emboss.Applications import Primer3Commandline

primerfile = '/Users/jared/Desktop/TestGenomes/PrimerTime/PrimerTimeDesigns/DengueVirus_47primerfile5.fasta'
primer_cl = Primer3Commandline()
primer_cl.set_parameter("-sequence", primerfile)
primer_cl.set_parameter("-outfile", '/Users/jared/Desktop/TestGenomes/PrimerTime/PrimerTimeDesigns/DengueVirus_47primerfile6.fasta')
primer_cl.set_parameter("-numreturn", "20")
primer_cl.set_parameter("-hybridprobe", "Y")

#use 1.1.4 version of primer3_core
#must add option to bio/emboss/applications.py
primer_cl.numnsaccepted = 2

result, messages = primer_cl()
