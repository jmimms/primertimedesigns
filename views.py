from django.shortcuts import render_to_response
#from Models.models import Script, Output
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.models import User
from django import forms
from django.shortcuts import render_to_response
from django.core.files import File
from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import csrf_exempt
from django.core.mail import *
from multiprocessing import Process
import subprocess
import sys
import os
import glob
import md5
import os.path
import imp
import traceback
import re
import time
import djcelery
from sets import Set
from celery import task
from PrimerTimeDesigns.tasks import spawnOneScript2
from PrimerTimeDesigns.tasks import spawnOneScript
from PrimerTimeDesigns.tasks import two
	
class ContactForm(forms.Form):
	name = forms.CharField(max_length=100)
	email = forms.EmailField(max_length=100)
	subject = forms.CharField(max_length=200)
	message = forms.CharField(widget=forms.Textarea(), max_length=500)
	
class SubmitForm(forms.Form):
	gene_region = forms.CharField(max_length=100,required=False)
	fader = forms.CharField(max_length=100,required=False)
	alpha = forms.CharField(max_length=100,required=False)
	alpha_new_value = forms.CharField(max_length=100,required=False)
	max_n = forms.CharField(max_length=2,required=False)
	max_primers = forms.CharField(max_length=2,required=False)
	cross_reactors = forms.CharField(max_length=10000,required=False)
	serotypes = forms.CharField(max_length=10000,required=False)
	beta_new_value = forms.CharField(max_length=100,required=False)
	high_divergent = forms.BooleanField(required=False)
	email = forms.EmailField(max_length=100)
	beta = forms.CharField(max_length=100,required=False)
	
# Create your views here.
@csrf_exempt
def index(request):
	if request.POST:
		form = SubmitForm(request.POST)
		if form.is_valid():
			cross_reactors = request.POST['cross_reactors']
			feedback = '<p id=pst_message>Query successfully submitted. Wait patiently for your report email.</p>'
			email = form.cleaned_data['email']
			beta = form.cleaned_data['beta']
			alpha = form.cleaned_data['alpha']
			fader = form.cleaned_data['fader']
			serotypes = form.cleaned_data['serotypes']
			gene_region = form.cleaned_data['gene_region']
			cross_reactors = form.cleaned_data['cross_reactors']
			high_divergent = form.cleaned_data['high_divergent']
			max_n = str(form.cleaned_data['max_n'])
			max_primers = str(form.cleaned_data['max_primers'])
			if beta  == 'Input a Virus Species':
				beta = ''
			if alpha == 'Input a Bacteria Species':
				alpha = ''
			if beta  == 'none':
				beta = ''
			if alpha == 'none':
				alpha = ''
			if fader == 'Input Additional Strains':
				fader = ''
			if serotypes == 'Input Serotypes':
				serotypes = ''
			if gene_region == 'Input Gene or Region':
				gene_region = ''
			if cross_reactors == 'Input Cross Reactor List':
				cross_reactors = ''
			if high_divergent == True:
				high_divergent = str(1)
			if high_divergent == False:
				high_divergent = str(0)
			if alpha:
				if not gene_region:
					feedback = '<p id=pst_message>Gene field in Advanced Options required for bacteria query.</p>'
					return render_to_response('index.html', locals())
			spawnOneScript.delay(gene_region,fader,alpha,max_n,max_primers,cross_reactors,serotypes,high_divergent,email,beta)
			feedback = '<p id=pst_message>Query successfully submitted. Wait patiently for your report email.</p>'
		else:
			feedback = '<p id=pst_message>Enter valid email and species.</p>'
		
		return render_to_response('index.html', locals())
	return render_to_response('index.html', locals())
def about(request):
	return render_to_response('about.html', locals())
def freewilly(request):
	return render_to_response('freewilly.html', locals())
def platypuspro(request):
	return render_to_response('platypuspro.html', locals())
def account(request):
	return render_to_response('account.html', locals())
	
@csrf_exempt
def contact(request):
	if request.POST:
		form = ContactForm(request.POST)
		if form.is_valid():
			message = "From: %s <%s>\r\nSubject:%s\r\nMessage:\r\n%s\r\n" % (
		        form.cleaned_data['name'],
		        form.cleaned_data['email'],
		        form.cleaned_data['subject'],
		        form.cleaned_data['message']
		    )
			mail_admins('Contact form', message, fail_silently=False)
			if request.is_ajax():
				return render_to_response('contact.html', locals())
			else:
				return render_to_response('contact.html', locals())
	else:
		form = ContactForm()
	return render_to_response('contact.html', locals())
    