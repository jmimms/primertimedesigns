from django.shortcuts import render_to_response
#from Models.models import Script, Output
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.models import User
from django import forms
from django.shortcuts import render_to_response
from django.core.files import File
from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import csrf_exempt
from django.core.mail import *
from multiprocessing import Process
import subprocess
import sys
import os
import glob
import md5
import os.path
import imp
import traceback
import re
import time
import djcelery
from sets import Set
from celery.task import task

@task
def spawnOneScript(gene_region,fader,alpha,max_n,max_primers,cross_reactors,serotypes,high_divergent,email,beta):
	if beta:
		species = beta
		bacteria = 1
	else:
		species = alpha
		bacteria = 0
	command = 'python OneScriptV1_1.py ' + '-e' + ' "' + email + '" ' + '-s ' + '"' + species + '" ' + '-g ' + '"' + gene_region + '" ' + '-n ' + '"' + max_n + '" ' + '-p ' + '"' + fader + '" ' + '-v ' + '"' + max_primers + '" ' + '-c ' + '"' + cross_reactors + '" ' + '-t ' + '"' + serotypes + '" ' + '-b ' + '"' + str(bacteria) + '" ' + '-y ' + '"' + high_divergent + '"' 
 	os.system(command)
 
@task
def two():
	a = 2 + 2