import os
from time import *


def spawnOneScript(gene_region,fader,alpha,max_n,max_primers,cross_reactors,serotypes,high_divergent,email,beta):
	if beta:
		species = beta
		bacteria = 1
	else:
		species = alpha
		bacteria = 0
	command = 'python OneScriptV1_1.py ' + '-e' + ' "' + email + '" ' + '-s ' + '"' + species + '" ' + '-g ' + '"' + gene_region + '" ' + '-n ' + '"' + max_n + '" ' + '-p ' + '"' + fader + '" ' + '-v ' + '"' + max_primers + '" ' + '-c ' + '"' + cross_reactors + '" ' + '-t ' + '"' + serotypes + '" ' + '-b ' + '"' + str(bacteria) + '" ' + '-y ' + '"' + high_divergent + '"' 
 	os.system(command)
 	
spawnOneScript('','','Human Adenovirus A','','','','','','jbmimms@gmail.com','')