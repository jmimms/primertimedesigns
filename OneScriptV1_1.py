#!/usr/bin/env python
#PrimerTime
# A script to automatically design primers and probes for any Prokaryote and human infecting virus, check them for cross reactivity based on user input, and produce tables and metrics for the analysis of these primers and probes
#copyright Jared Mimms 2012

#imports and libaries
import Bio
import sys
from random import *
from Bio.Blast import NCBIWWW
from Bio import Entrez, SeqIO
import re
import unicodedata
import ast
import httplib
from bs4 import BeautifulSoup
from time import sleep
from Bio.Alphabet import generic_dna
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Align import MultipleSeqAlignment
from Bio import Motif
from Bio.Alphabet import IUPAC
from bs4 import BeautifulStoneSoup
from decimal import *
from Bio.Application import *
from Bio.Emboss import Primer3
from Bio.Align.Applications import MuscleCommandline
from Bio.Align.Applications import ClustalwCommandline
from StringIO import StringIO
from Bio import AlignIO
from multiprocessing import *
from boto.ec2.connection import EC2Connection
import paramiko
import os
import string
from xlrd import *
from xlwt import *
from optparse import OptionParser
import smtplib
import mimetypes
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.MIMEAudio import MIMEAudio
from email.MIMEImage import MIMEImage
from email.Encoders import encode_base64
from Bio.Emboss.Applications import Primer3Commandline


#craft query, get result ids, build fasta file for all sequences
def craft_query(species,gene,cross_reactors,bacteria,ret_allowed,species_list,st,genefile,book):
	entrez_search_string = ''
	if gene and bacteria:
		c_counter = 0
		if len(species_list) > 1:
			entrez_search_string = species + ' ' + gene + " AND " + gene + "[TI]" + " AND " + "(" + species + "[Orgn]"
			for item in species_list:
				if c_counter == len(species_list)-1:
					entrez_search_string += " OR " + item + "[Orgn]" + ')'
				else:
					entrez_search_string += " OR " + item + "[Orgn]"
				c_counter += 1
		else:
			entrez_search_string = species + "[Orgn]" + " AND " + gene + "[Gene]"
	elif st:
		spec_count = 0
		for spec in species_list:
			for strains in st[spec_count]:
				if (strains == st[spec_count][-1] and spec == species_list[-1]):
					entrez_search_string += '(\"' + spec + '\" \"serotype ' + str(strains) + '\" \"complete genome\"' + ' AND ' + '\"' + species + '\" \"' + str(strains) + '\"[TI])'
				else:
					entrez_search_string += '(\"' + spec + '\" \"serotype ' + str(strains) + '\" \"complete genome\"' + ' AND ' + '\"' + species + '\" \"' + str(strains) + '\"[TI]) OR '
			spec_count += 1		 
	else:
		c_counter = 0
		entrez_search_string = "\"" + species + "\" " + "(\"complete genome\"[TI]" + " AND " + species + "[Orgn]" + ' NOT ' + '(Murine[TI] OR mouse[TI] OR Porcine[TI] OR Simian[TI])'
		if bacteria == 0 and gene:
			if species_list:
				entrez_search_string = "\"" + species + "\"[TI] " + gene + " AND (" 
				for item in species_list:
					if item == species_list[-1]:
						entrez_search_string += item + "[Orgn]"
					else:
						entrez_search_string += item + "[Orgn]" + ' OR '
				entrez_search_string += ')' + ' NOT ' + '(Murine[TI] OR Porcine[TI] OR Simian[TI] OR complete genome[TI] OR mrna[TI])' 
			else:
				entrez_search_string = species + ' ' + gene + " AND " + species + "[Orgn]" + ' NOT ' + '(Murine[TI] OR "mouse"[TI] OR Porcine[TI] OR Simian[TI] OR complete genome[TI] OR mrna[TI])' 
					
		elif len(species_list) > 1:
			if gene:
				entrez_search_string = "\"" + species + "\"[TI] " + gene
			else:
				entrez_search_string = "\"" + species + "\" " + "(\"complete genome\"[TI])"
			for item in species_list:
				if c_counter == 0:
					entrez_search_string += " AND " + '(' + item + "[Orgn]" 
				elif c_counter == len(species_list)-1:
					entrez_search_string += " OR " + item + "[Orgn]" + ')'
				else:
					entrez_search_string += " OR " + item + "[Orgn]"
				c_counter += 1
			entrez_search_string += " AND " + species + "[TI]" + ' NOT ' + '(Murine[TI] OR mouse[TI] OR Porcine[TI] OR Simian[TI])'

	if gene and bacteria:
		b = 1
		while b > 0:
			try:
				results_handle = Entrez.esearch(db="gene",retmax=ret_allowed,term=entrez_search_string)
				b = 0
			except:
				sleep(1)
				b = 1
	elif bacteria:
		b = 1
		while b > 0:
			try:
				results_handle = Entrez.esearch(db="nucleotide",retmax=ret_allowed,term=entrez_search_string)
				b = 0
			except:
				sleep(1)
				b = 1	
	else:
		b = 1
		while b > 0:
			try:
				results_handle = Entrez.esearch(db="nucleotide",retmax=ret_allowed,term=entrez_search_string)
				b = 0
			except:
				sleep(1)
				b = 1
	results = Entrez.read(results_handle)
	result_ids = results["IdList"]

	fff=open(genefile, "w")
	e = 1

	result_array = []
	
	#break results into manageable chunks of 100 to feed into efetch to retrieve sequences
	if len(result_ids) > 400:
		one_hundred_counter = 0
		hhgregg = 0
		while hhgregg == 0:
			if len(result_ids)-100 < one_hundred_counter:
				result_array.append(result_ids[one_hundred_counter:])
				hhgregg = 1
			else:
				result_array.append(result_ids[one_hundred_counter:one_hundred_counter+100])
				if len(result_ids) == one_hundred_counter:
					hhgregg = 1
			one_hundred_counter += 100
	else:
		result_array.append(result_ids)
		
	#if bacteria and gene find and convert ID from gene to nucleotide databases with indexes and feed them into efetch
	#else feed already obtained IDs into efetch
	if bacteria and gene:
		for item in result_array:
			itemthings = []
			for itemid in item:
				conn = httplib.HTTPConnection('www.ncbi.nlm.nih.gov')
	
				url2 = '/gene?term=' + str(itemid)
				y=0
				while y==0:
					try:
						conn.request("GET",url2)
						res= conn.getresponse()
						y=1
					except:
						y=0 
				res = res.read()
				soup = BeautifulSoup(res)
		
				rez = soup.find("p", "note-link").findAll("a")
				parse_link = []
				link_count = 0
		
				for link in rez:
					if link_count == 1:
						fast_link = link.get('href')
					link_count += 1
				
				m = re.search('from\=(.*?)\&', fast_link)
				sstart = m.group(0)
				sstart = int(re.sub("[^0-9]", "", sstart))
				n = re.search('to\=(.*?)\&', fast_link)
				if n:
					k = 1
				else:
					n = re.search('to\=.*', fast_link)
				sstop = n.group(0)
				sstop = int(re.sub("[^0-9]", "", sstop))
				
				gi = re.search('nuccore\/(.*?)\?', fast_link)
				number = gi.group(0)
				number = number[8:-1]
				handle = Entrez.efetch(db='nucleotide',id=number, seq_start=sstart, seq_stop=sstop, rettype='fasta', retmode='text')
				for thing in handle:
					fff.write(thing)
				
	else:
		for item in result_array:
			handle = Entrez.efetch(db='nucleotide',id=item, rettype='fasta', retmode='text')
			for thing in handle:
				fff.write(thing)
	
	handle.close()
	
	done = 1
	
	fff.close()
	final_file = []
	for record in SeqIO.parse(genefile, "fasta"):
		if re.search('mouse/',record.description):
			a = 1
		elif re.search('dog/',record.description):
			a = 1 
		elif re.search('swine/',record.description):
			a = 1
		elif re.search('pig/',record.description):
			a = 1
		elif re.search('shashlik/',record.description):
			a = 1
		elif re.search('Bo/',record.description):
			a = 1
		else:
			final_file.append('>' + record.description + '\n' + record.seq + '\n\n')
	tk = open(genefile, "w")
	for item in final_file:
		tk.write(str(item))
	tk.close
			
	return result_ids

#run alignment on Amazon cloud instance
def cloud_compute(ins,outs,clustalint,ins2,outs2,clustalint2):
	conn = EC2Connection('AKIAJG76Q2IC33SWVBPQ', 'RKOAmVj2oLk27T2ypkPvN+G3WGafYhCOHHYMIKEp')
	instan = conn.get_all_instances()[0]
	inst = instan.instances[0]
	inst.start()
	while inst.state == 'stopped':
		inst.update()
	while inst.state == 'pending':
		inst.update()
		sleep(10)
	ssh = paramiko.SSHClient()
	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	reservations = conn.get_all_instances(instance_ids=[inst.id])
	instance = reservations[0].instances[0]
	ssh.connect(instance.public_dns_name, username='ubuntu', password='Cr102089', key_filename=os.path.expanduser('/Users/jared/Desktop/TestGenomes/PrimerTime/PrimerTimeDesigns/Keys/cloudman_key_pair-key.pem'))
	ftp = ssh.open_sftp()
	ftp.put('/Users/jared/Desktop/TestGenomes/PrimerTime/PrimerTimeDesigns/server.py','server.py')
	ftp.put(ins,ins2)
	ssh.exec_command('python server.py' + ' ' + ins2 + ' ' + clustalint2 + ' ' + outs2)
	seq_count = 0
	for seq_record in SeqIO.parse(ins, "fasta", alphabet=IUPAC.unambiguous_dna):
		seq_count += 1
	x = 1
	while x == 1:
		try:
			seq2_count = 0
			ftp.get(outs2,outs)
			for seq_record in SeqIO.parse(outs, "fasta", alphabet=IUPAC.unambiguous_dna):
				seq2_count += 1
			if seq2_count == seq_count:
				x = 0
			else:
				x = 1
			sleep(3)
		except:
			x = 1
	
#perform clustal alignment on infile (FASTA format) and return an aligned array for further processing
def cw_alignment(ins, outs, clustalint, cloud_computer,book,ins2,outs2,clustalint2):
	pre_consensus = []
	seq_count = 0
	seq_length = 0
	for seq_record in SeqIO.parse(ins, "fasta", alphabet=IUPAC.unambiguous_dna):
		seq_length += len(seq_record.seq)
		seq_count += 1
	if seq_count > 1:
		if cloud_computer:
			if seq_length < 200000:
				clustler = ClustalwCommandline("clustalw", infile=ins, outfile=clustalint)
				stdout, stderr = clustler()
				cn = SeqIO.convert(clustalint, "clustal", outs, "fasta")
			else:
				cloud_compute(ins,outs,clustalint,ins2,outs2,clustalint2)
		else:
			clustler = ClustalwCommandline("clustalw", infile=ins, outfile=clustalint)
			stdout, stderr = clustler()
			cn = SeqIO.convert(clustalint, "clustal", outs, "fasta")
		pre_consensus = []
		for seq_record in SeqIO.parse(outs, "fasta", alphabet=IUPAC.unambiguous_dna):
			pre_consensus.append(seq_record.seq)
		return pre_consensus
	else:
		a=open(outs, 'w')
		b=open(ins, 'r')
		for line in b:
			a.write(line)
		a.close
		b.close
		for seq_record in SeqIO.parse(ins, "fasta", alphabet=IUPAC.unambiguous_dna):
			pre_consensus.append(seq_record.seq)
		return pre_consensus
		
#create degerate consensus
def degen_sequence(cons_pre):
	consensus_counter = 0
	item_counter = 0
	consensus_string = ''
	total_items = len(cons_pre)
	for letter in cons_pre[0]:
		a_count = 0
		c_count = 0
		g_count = 0
		t_count = 0
		u_count = 0
		letter_item = ''
		for item in cons_pre:
			if item[consensus_counter] == 'A':
				a_count += 1
			elif item[consensus_counter] == 'C':
				c_count += 1
			elif item[consensus_counter] == 'G':
				g_count += 1
			elif item[consensus_counter] == 'T':
				t_count += 1
			elif item[consensus_counter] == 'U':
				u_count += 1
		prop_a = Decimal(a_count)/Decimal(total_items)
		prop_c = Decimal(c_count)/Decimal(total_items)
		prop_g = Decimal(g_count)/Decimal(total_items)
		prop_t = Decimal(t_count)/Decimal(total_items)
		prop_u = Decimal(u_count)/Decimal(total_items)
		compare_whole = Decimal(1)
		compare_nine = Decimal(.9)
		if prop_a == compare_whole:
			letter_item = 'A'
		elif prop_a >= compare_nine:
			letter_item = 'a'
		if prop_c == compare_whole:
			letter_item = 'C'
		elif prop_c >= compare_nine:
			letter_item = 'c'
		if prop_g == compare_whole:
			letter_item = 'G'
		elif prop_g >= compare_nine:
			letter_item = 'g'
		if prop_t == compare_whole:
			letter_item = 'T'
		elif prop_t >= compare_nine:
			letter_item = 't'
		if prop_u == compare_whole:
			letter_item = 'U'
		elif prop_u >= compare_nine:
			letter_item = 'u'
		if prop_a >= Decimal(.1):
			if prop_a <= Decimal(.89):
				if ((prop_t >= Decimal(.1)) or (prop_u >= Decimal(.1))): 
					if ((prop_t <= Decimal(.89)) or (prop_u <= Decimal(.89))):
						if (((prop_t+prop_a) > Decimal(.90)) or ((prop_u+prop_a) > Decimal(.90))):
							letter_item = 'w'
		if prop_g >= Decimal(.1):
			if prop_g <= Decimal(.89):
				if prop_c >= Decimal(.1): 
					if prop_c <= Decimal(.89):
						if (prop_c+prop_g) > Decimal(.90):
							letter_item = 's'
		if prop_g >= Decimal(.1):
			if prop_g <= Decimal(.89):
				if prop_a >= Decimal(.1): 
					if prop_a <= Decimal(.89):
						if (prop_g+prop_a) > Decimal(.90):
							letter_item = 'r'
		if ((prop_t >= Decimal(.1)) or (prop_u >= Decimal(.1))):
			if ((prop_t <= Decimal(.89)) or (prop_u <= Decimal(.89))):
				if prop_c >= Decimal(.1): 
					if prop_c <= Decimal(.89):
						if (((prop_t+prop_c) > Decimal(.90)) or ((prop_u+prop_c) > Decimal(.90))):
							letter_item = 'y'
		if ((prop_t >= Decimal(.1)) or (prop_u >= Decimal(.1))):
			if ((prop_t <= Decimal(.89)) or (prop_u <= Decimal(.89))):
				if prop_g >= Decimal(.1): 
					if prop_g <= Decimal(.89):
						if (((prop_t+prop_g) > Decimal(.90)) or ((prop_u+prop_g) > Decimal(.90))):
							letter_item = 'k'
		if prop_a >= Decimal(.1):
			if prop_a <= Decimal(.89):
				if prop_c >= Decimal(.1): 
					if prop_c <= Decimal(.89):
						if (prop_c+prop_a) > Decimal(.90):
							letter_item = 'm'
		if letter_item == '':
			letter_item = 'n'
		consensus_string += letter_item
		consensus_counter += 1
	return consensus_string
	
#clean degenerate bases to Ns required for Primer3 input
def clean_consensus(consensus_string):
	fin_consensus_string = ''
	for letter in consensus_string:
		if letter not in ['A','a','C','c','G','g','T','t','U','u']:
			fin_consensus_string += 'n'
		else:
			fin_consensus_string += letter
	return fin_consensus_string

#perform all functions with primers
def primer_call(primer_query,consensus_string,bacteria,fin_hit_id,output2,alignmentfile,max_n,high_divergent,similarity,primer_set_max,primerfile,primerfileout,book):
	if high_divergent:
		nuc_count = 0
		new_string = ''
		for nucleotide in consensus_string:
			if nucleotide == 'w':
				new_string += 'A'
			elif nucleotide == 's':
				new_string += 'C'
			elif nucleotide == 'm':
				new_string += 'A'
			elif nucleotide == 'k':
				new_string += 'G'
			elif nucleotide == 'r':
				new_string += 'G'
			elif nucleotide == 'y':
				new_string += 'T'
			else:
				new_string += consensus_string[nuc_count]
			nuc_count += 1
		primer_query = new_string
		
	ff=open(primerfile, "wr")
	primer_query = str(primer_query)
	ff.write(primer_query)
	ff.close()
	len_primer_seq = len(primer_query)
	divis = 50
	end_seq = len(primer_query) - divis
	#web interface for Primer3
	#downside-don't want to trust outside programs
	#potential url changes
	
	left_primer_head_elements = []
	left_primer_sequence_elements = []
	left_primer_info_elements = []
	oligo_head_elements = []
	oligo_sequence_elements = [] 
	oligo_info_elements = []
	right_primer_head_elements = [] 
	right_primer_sequence_elements = [] 
	right_primer_info_elements = []
	
	primer_cl = Primer3Commandline()
	
	primer_cl.set_parameter("-sequence", primerfile)
	primer_cl.set_parameter("-outfile", primerfileout)
	primer_cl.set_parameter("-numreturn", "20")
	primer_cl.set_parameter("-hybridprobe", "Y")
	
	#use 1.1.4 version of primer3_core
	#must add option to bio/emboss/applications.py
	primer_cl.numnsaccepted = 2
	result, messages = primer_cl()
	
	output_handle = open(primerfileout,"r")
	primer_record = Primer3.read(output_handle)
	
	prime_count = 0
	
	for item in primer_record.primers:
		oligo_sequence_elements.append(item.internal_seq)
		oligo_head_elements.append('Probe_' + str(prime_count))
		oligo_info_elements.append('Start:\t' + str(item.internal_start) + ' Length:\t' + str(item.internal_length) + ' Tm:\t' + str(item.internal_tm) + ' GC:\t' + str(item.internal_gc))
		left_primer_head_elements.append('Primer_' + str(prime_count) + '_F')
		left_primer_sequence_elements.append(item.forward_seq)
		left_primer_info_elements.append('Start:\t' + str(item.forward_start) + ' Length:\t' + str(item.forward_length) + ' Tm:\t' + str(item.forward_tm) + ' GC:\t' + str(item.forward_gc))
		right_primer_head_elements.append('Primer_' + str(prime_count) + '_R') 
		right_primer_sequence_elements.append(item.reverse_seq) 
		right_primer_info_elements.append('Start:\t' + str(item.reverse_start) + ' Length:\t' + str(item.reverse_length) + ' Tm:\t' + str(item.reverse_tm) + ' GC:\t' + str(item.reverse_gc))
		
		prime_count += 1
		
	output_handle.close()
	
	#formatting/listing
	item_count = 0
	left_primers = []
	right_primers = []
	oligos = []
	left_primer_heads = []
	right_primer_heads = []
	oligo_heads =[]
	final_sortable_left = []
	final_sortable_oligo = []
	final_sortable_right = []
	final_sortable_left_labels = []
	final_sortable_oligo_labels = []
	final_sortable_right_labels = []
	column_index = 0 
	
	sheet2 = book.get_sheet(0)
	sheet3 = book.get_sheet(1)
	counter = 9
	counter2 = 0
	
	for item in left_primer_head_elements:
		
		sheet2.write(counter,0,left_primer_head_elements[item_count],easyxf('font: bold True;'))
		sheet3.write(counter2,0,left_primer_head_elements[item_count],easyxf('font: bold True;'))
		counter += 1
		counter2 += 1
		
		sheet2.write(counter,0,left_primer_sequence_elements[item_count])
		sheet3.write(counter2,0,left_primer_sequence_elements[item_count])
		counter += 1
		counter2 += 1
		
		sheet2.write(counter,0,left_primer_info_elements[item_count])
		sheet3.write(counter2,0,left_primer_info_elements[item_count])
		counter += 1
		counter += 1
		counter2 += 1
		counter2 += 1
		
		left_primers.append(left_primer_sequence_elements[item_count])
		left_primer_heads.append(left_primer_head_elements[item_count])
		
		#reading in sequences again to get records for primer comparison pulling - may need 
		#get start of primer of consensus sequence
		#degenerate sequence = dgen_seq
		
		length_pull = len(left_primer_sequence_elements[item_count])
		
		#get position from string
		primer_match = re.search('[0-9]+', left_primer_info_elements[item_count])
		pre_pre = primer_match.group(0)
		primer_start = int(pre_pre) - 1
		end_length = primer_start + length_pull
		left_primer_consensus = consensus_string[primer_start:end_length]
		
		input_handle = open(alignmentfile, "rU")
		
		rec_count = 0
		sortable_left = []
		final_sortable_left_labels_first = []
		sortable_oligo = []
		final_sortable_oligo_labels_first = []
		sortable_right = []
		final_sortable_right_labels_first = []
		for record in SeqIO.parse(input_handle, "fasta"):
			left_primer_rec = record.seq[primer_start:end_length]
			for item in fin_hit_id:
				matcher = ''
				for letter in record.id:
					if letter == '|':
						matcher += '\|'
					elif letter == '.':
						matcher += '\.'
					else:
						matcher += letter
				compare = re.search(matcher,item)
				if compare:
						aaa = str(left_primer_rec) + ' | ' + str(item)
						sheet3.write(counter2,0,aaa)
						sortable_left.append(left_primer_rec)
						final_sortable_left_labels_first.append(item)
						counter2 += 1
			rec_count += 1
		final_sortable_left_labels.append(final_sortable_left_labels_first)
		final_sortable_left.append(sortable_left)
		input_handle.close()
		sheet2.write(counter,0,'Forward primer consensus:')
		sheet3.write(counter2,0,'Forward primer consensus:')
		counter += 1
		counter2 += 1
		
		sheet2.write(counter,0,left_primer_consensus)
		sheet3.write(counter2,0,left_primer_consensus)
		counter += 1
		counter2 += 1
		counter += 1
		counter2 += 1
		
		sheet2.write(counter,0,oligo_head_elements[item_count],easyxf('font: bold True;'))
		sheet3.write(counter2,0,oligo_head_elements[item_count],easyxf('font: bold True;'))
		counter += 1
		counter2 += 1
		sheet2.write(counter,0,oligo_sequence_elements[item_count])
		sheet3.write(counter2,0,oligo_sequence_elements[item_count])
		counter += 1
		counter2 += 1
		sheet2.write(counter,0,oligo_info_elements[item_count])
		sheet3.write(counter2,0,oligo_info_elements[item_count])
		counter += 1
		counter2 += 1
		counter += 1
		counter2 += 1

		oligos.append(oligo_sequence_elements[item_count])
		oligo_heads.append(oligo_head_elements[item_count])
		
		length_pull = len(oligo_sequence_elements[item_count])
		primer_match = re.search('[0-9]+', oligo_info_elements[item_count])
		pre_pre = primer_match.group(0)
		primer_start = int(pre_pre) - 1
		end_length = primer_start + length_pull
		oligo_consensus = consensus_string[primer_start:end_length]
		
		input_handle = open(alignmentfile, "rU")
		rec_count = 0
		for record in SeqIO.parse(input_handle, "fasta"):
			oligo_rec = record.seq[primer_start:end_length]
			for item in fin_hit_id:
				matcher = ''
				for letter in record.id:
					if letter == '|':
						matcher += '\|'
					elif letter == '.':
						matcher += '\.'
					else:
						matcher += letter
				compare = re.search(matcher,item)
				if compare:
						aaa = str(oligo_rec) + ' | ' + str(item)
						sheet3.write(counter2,0,aaa)
						sortable_oligo.append(oligo_rec)
						final_sortable_oligo_labels_first.append(item)
						counter2 += 1
			rec_count += 1
		final_sortable_oligo_labels.append(final_sortable_oligo_labels_first)
		final_sortable_oligo.append(sortable_oligo)
		input_handle.close()
		
		sheet2.write(counter,0,'Oligo consensus:')
		sheet3.write(counter2,0,'Oligo consensus:')
		counter += 1
		counter2 += 1
		
		sheet2.write(counter,0,oligo_consensus)
		sheet3.write(counter2,0,oligo_consensus)
		counter += 1
		counter2 += 1
		counter += 1
		counter2 += 1
		
		sheet2.write(counter,0,right_primer_head_elements[item_count],easyxf('font: bold True;'))
		sheet3.write(counter2,0,right_primer_head_elements[item_count],easyxf('font: bold True;'))
		counter += 1
		counter2 += 1
		
		sheet2.write(counter,0,right_primer_sequence_elements[item_count])
		sheet3.write(counter2,0,right_primer_sequence_elements[item_count])
		counter += 1
		counter2 += 1
		
		sheet2.write(counter,0,right_primer_info_elements[item_count])
		sheet3.write(counter2,0,right_primer_info_elements[item_count])
		counter += 1
		counter2 += 1
		counter += 1
		counter2 += 1
		
		right_primers.append(right_primer_sequence_elements[item_count])
		right_primer_heads.append(right_primer_head_elements[item_count])
		
		length_pull = len(right_primer_sequence_elements[item_count])
		primer_match = re.search('[0-9]+', right_primer_info_elements[item_count])
		pre_pre = primer_match.group(0)
		
		primer_start = int(pre_pre) - 1
		end_length = primer_start + length_pull
		
		dgen_right_maker = []
		right_primer_consensus = consensus_string[primer_start:end_length]
	
		rec_count = 0
		input_handle = open(alignmentfile, "rU")
		total_seq = 0
		for record in SeqIO.parse(input_handle, "fasta"):
			right_primer_rec = record.seq[primer_start:end_length]
			for item in fin_hit_id:
				matcher = ''
				for letter in record.id:
					if letter == '|':
						matcher += '\|'
					elif letter == '.':
						matcher += '\.'
					else:
						matcher += letter
				compare = re.search(matcher,item)
				if compare:
						aaa = str(right_primer_rec.reverse_complement()) + ' | ' + str(item)
						sheet3.write(counter2,0,aaa)
						sortable_right.append(right_primer_rec.reverse_complement())
						final_sortable_right_labels_first.append(item)
						counter2 += 1
			rec_count += 1
			dgen_right_maker.append(right_primer_rec.reverse_complement())
			total_seq += 1
			
		input_handle.close()
		final_sortable_right_labels.append(final_sortable_right_labels_first)
		final_sortable_right.append(sortable_right)
			
		final_sortable_right.append(sortable_right)
		
		sheet2.write(counter,0,'Reverse primer consensus:')
		sheet3.write(counter2,0,'Reverse primer consensus:')
		counter += 1
		counter2 += 1
		
		#need to relalign above and generate new degenerate
		dgen_right = degen_sequence(dgen_right_maker)
		
		sheet2.write(counter,0,dgen_right)
		sheet3.write(counter2,0,dgen_right)
		counter += 1
		counter2 += 1
		del dgen_right_maker[:]
		sheet2.write(counter,0,'***********')
		sheet3.write(counter2,0,'***********')
		counter += 1
		counter2 += 1
		item_count += 1
	
	#final sortable right contains array for each primer set of all sets
	final_key_full_final = []
	final_labels_full_full = []
	
	#keys for each primer set
	final_key_left = []
	final_key_right = []
	final_key_oligo = []
	
	#sort labels for each primer set
	final_sort_left = []
	final_sort_right = []
	final_sort_oligo = []
	
	#left_sort in parallel with these arrays - thus 1 corresponds to one
	final_left_sort = []
	final_right_sort = []
	final_oligo_sort = []
		
	fin_count = 0
	
	#highly divergent features
	#for each primer set
	if high_divergent:
		order_num = 0
		key_num = 0
		orders_num = 0
		orders_key = book.add_sheet('HighDivergentOrder')
		order = book.add_sheet('HighDivergentOrderOverview')
		order_key = book.add_sheet('HighDivergentStrainKey')
		for item in left_primers:
			left_sort = []
			right_sort = []
			oligo_sort = []
			
			order.write(order_num,0,left_primer_heads[fin_count],easyxf('font: bold True;'))
			order_num += 1
			order.write(order_num,0,item)
			order_num += 1
			order.write(order_num,0,"Divergent sequences:")
			order_num += 1

			
			correct = 0
			thing_counter = 0
			
			#for thing in each primer set
			#thing is each primer in set
			#for each thing run in set if independent group label with key of group sequence
			#final sortable labels in parallel with thing
			#input 
		
			key_array = []
			sort_array = []
			for thing in final_sortable_left[fin_count]:
				if thing_counter > 0:
					#left_sort includes all group names
					for seq in left_sort:
						active = 0
						let_count = 0
						correct = 0
						for letter in seq:
							if letter == thing[let_count]:
								correct += 1
							let_count += 1	
						proportion = Decimal(correct)/Decimal(let_count)
						if proportion > similarity or Decimal(correct/let_count) == similarity:
							active = 1
							key_array.append(seq)
							sort_array.append(final_sortable_left_labels[fin_count][thing_counter])
							
					if active == 0:
						nogood= 0 
						for item in left_sort:
							if str(item) == str(thing):
								nogood = 1
						if nogood == 0:
							left_sort.append(str(thing))
						key_array.append(str(thing))
						sort_array.append(final_sortable_left_labels[fin_count][thing_counter])
						
				else:
					nogood = 0 
					for item in left_sort:
						if str(item) == str(thing):
							nogood = 1
					if nogood == 0:
						left_sort.append(str(thing))
							
					key_array.append(str(thing))
					sort_array.append(final_sortable_left_labels[fin_count][thing_counter])
				thing_counter += 1
	
			sort_count = 0
			for sequence in left_sort:
				order.write(order_num,0,'Group ' + str(sort_count) + ': ' + str(sequence))
				order_num += 1
				sort_count += 1
				
			#this is by primer set
			final_key_left.append(key_array)
			final_sort_left.append(sort_array)
			final_left_sort.append(left_sort)
				
			sequence_counter = 0
			
			order_num += 1
			order.write(order_num,0,oligo_heads[fin_count],easyxf('font: bold True;'))
			order_num += 1
			order.write(order_num,0,oligos[fin_count])
			order_num += 1
			order.write(order_num,0,"Divergent sequences:")
			order_num += 1
			
			key_array = []
			sort_array = []
			thing_counter = 0
			correct = 0
			thing_counter = 0
			
			for thing in final_sortable_oligo[fin_count]:
				if thing_counter > 0:
					for seq in oligo_sort:
						active = 0
						let_count = 0
						correct = 0
						for letter in seq:
							if letter == thing[let_count]:
								correct += 1
							let_count += 1
						proportion = Decimal(correct)/Decimal(let_count)
						if proportion > similarity or Decimal(correct/let_count) == similarity:
							active = 1
							key_array.append(seq)
							sort_array.append(final_sortable_oligo_labels[fin_count][thing_counter])
	
					if active == 0:
						nogood= 0 
						for item in oligo_sort:
							if str(item) == str(thing):
								nogood = 1
						if nogood == 0:
							oligo_sort.append(str(thing))
						key_array.append(str(thing))
						sort_array.append(final_sortable_oligo_labels[fin_count][thing_counter])
	
				else:
					nogood= 0 
					for item in oligo_sort:
						if str(item) == str(thing):
							nogood = 1
					if nogood == 0:
						oligo_sort.append(str(thing))
					key_array.append(str(thing))
					sort_array.append(final_sortable_oligo_labels[fin_count][thing_counter])
				thing_counter += 1
			
			final_key_oligo.append(key_array)
			final_sort_oligo.append(sort_array)
			final_oligo_sort.append(oligo_sort)
			
			sort_count = 0
			for sequence in oligo_sort:
				order.write(order_num,0,'Group ' + str(sort_count) + ': '  + str(sequence))
				order_num += 1
				sort_count += 1
				
			order_num += 1
			order.write(order_num,0,right_primer_heads[fin_count],easyxf('font: bold True;'))
			order_num += 1
			order.write(order_num,0,right_primers[fin_count])
			order_num += 1
			order.write(order_num,0,"Divergent sequences:")
			order_num += 1
			
			key_array = []
			sort_array = []
			thing_counter = 0
			for thing in final_sortable_right[fin_count]:
				if thing_counter > 0:
					for seq in right_sort:
						active = 0
						let_count = 0
						correct = 0
						for letter in seq:
							if letter == thing[let_count]:
								correct += 1
							let_count += 1
						proportion = Decimal(correct)/Decimal(let_count)
						if proportion > similarity or Decimal(correct/let_count) == similarity:
							active = 1
							key_array.append(seq)
							sort_array.append(final_sortable_right_labels[fin_count][thing_counter])
	
					if active == 0:
						nogood= 0 
						for item in right_sort:
							if str(item) == str(thing):
								nogood = 1
						if nogood == 0:
							right_sort.append(str(thing))
						key_array.append(str(thing))
						sort_array.append(final_sortable_right_labels[fin_count][thing_counter])
	
				else:
					nogood= 0 
					for item in right_sort:
						if str(item) == str(thing):
							nogood = 1
					if nogood == 0:
						right_sort.append(str(thing))
					key_array.append(str(thing))
					sort_array.append(final_sortable_right_labels[fin_count][thing_counter])
				thing_counter += 1
				
			final_key_right.append(key_array)
			final_sort_right.append(sort_array)
			final_right_sort.append(right_sort)
			
			sort_count = 0
			count_num = 0
			for sequence in right_sort:
				order.write(order_num,0,'Group ' + str(sort_count) + ': ' + str(sequence))
				order_num += 1
				sort_count += 1
				count_num += 1
				
			fin_count += 1
			
			order.write(order_num,0,'**********')
			order_num += 1
		
		#need two arrays one for each primer set then add that on to final
		set_array_left = []
		set_array_oligo = []
		set_array_right = []
		
		counter = 0
		for i in range(0,len(final_key_left)):
			fin_counter = 0
			final_array = []
			for item in final_left_sort[counter]:
				temp_array = []
				temp_counter = 0
				for sequence in final_key_left[counter]:
					if item == sequence:
						temp_array.append(final_sort_left[counter][temp_counter])
					temp_counter += 1
				#append number of group for identification followed by all array objects
				#array of all final_key - group matchups in primer group in group set
				#final array for each group
				final_array.append(temp_array)
				fin_counter += 1
			set_array_left.append(final_array)
			counter += 1 
			
		counter = 0
		for i in range(0,len(final_key_oligo)):
			fin_counter = 0
			final_array = []
			for item in final_oligo_sort[counter]:
				temp_array = []
				temp_counter = 0
				for sequence in final_key_oligo[counter]:
					if item == sequence:
						temp_array.append(final_sort_oligo[counter][temp_counter])
					temp_counter += 1
				#append number of group for identification followed by all array objects
				#array of all final_key - group matchups in primer group in group set
				#final array for each group
				final_array.append(temp_array)
				fin_counter += 1
			set_array_oligo.append(final_array)
			counter += 1 
			
		counter = 0
		for i in range(0,len(final_key_right)):
			fin_counter = 0
			final_array = []
			for item in final_right_sort[counter]:
				temp_array = []
				temp_counter = 0
				for sequence in final_key_right[counter]:
					if item == sequence:
						temp_array.append(final_sort_right[counter][temp_counter])
					temp_counter += 1
				#append number of group for identification followed by all array objects
				#array of all final_key - group matchups in primer group in group set
				#final array for each group
				final_array.append(temp_array)
				fin_counter += 1
			set_array_right.append(final_array)
			counter += 1 
			
		#add array to find out which
		set_num = 0
		
		final_data_left = []
		final_data_oligo = []
		final_data_right = []
		
		for item in set_array_left:
			order_key.write(key_num,0,'**********')
			key_num += 1
			order_key.write(key_num,0,'Primer set ' + str(set_num),easyxf('font: bold True;'))
			key_num += 1
			order_key.write(key_num,0,'Forward primer set key: ',easyxf('font: bold True;'))
			key_num += 1
			group_num = 0
			index_num = 0
			
			#eatch thing contains an array for each group with GI number and other information need to check for replicas make new array to put all sequence names
			#make final print array
			#rearrange arrays according to size with group numbers labeled concurrently
			
			thing_unique_array = []
			check_array = []
			
			#rearrange arrays (thing) according to size with group numbers labeled concurrently keep old list, run for loop through to get original index, place original index in new array
			
			group_number = []
			item2 = item[:]
			item2.sort(lambda x,y: cmp(len(x), len(y)))
			item2.reverse()
			
			for itemed in item2:
				group_number.append(item.index(itemed))
			
			#Put uniques for each forward, oligo and reverse for each primer set in arrays
				#for each primer_set for each unique set triple array 
				#primer_set -> unique group -> unique individual
			#Put group numbers in parallel arrays
				#for each primer group for each group triple array
				#primer_set -> group_set -> group individual
			
			#for each primer set
			#Sort uniques by size - link uniques and group numbers
			#print top ten of these pairs
			#allow user input for number of primers - count out top N primer sets for forward, oligo, and reverse - count up uniques for those groups, divide by total strains found. 
			
			#do this with forward, oligo, and reverse simulataneously
			
			final_data_1_left = []
			final_data_1_oligo = []
			final_data_1_right = []
			
			for thing in item2:
				#array for printing additon
				print_array = []
				unique_array = []
				
				#for each group
				for left in thing:
					if left not in check_array:	
						unique_array.append(left)
						print_array.append(left)	
					check_array.append(left)
				final_data_1_left.append((unique_array,group_number[group_num]))
			#array for each group
			#for item in list of all sequences if item in list equals left in thing add group number to item in list group array	
				
				order_key.write(key_num,0,'Group: ' + str(group_number[group_num]) + '	' + str(len(thing)) + ' total strains' + '	' + str(len(unique_array)) + ' unique strains')
				key_num += 1
				
				for pk in print_array:
					order_key.write(key_num,0,pk)
					key_num += 1
					
				key_num += 1
				index_num += 1
				thing_unique_array.append(print_array)
				group_num += 1
				
			order_key.write(key_num,0,'Oligo set key: ',easyxf('font: bold True;'))
			key_num += 1
			group_num = 0
			index_num = 0
			thing_unique_array = []
			check_array = []
			
			final_data_left.append(final_data_1_left)
			
			item2 = []
			group_number = []
			item2 = set_array_oligo[set_num][:]
			item2.sort(lambda x,y: cmp(len(x), len(y)))
			item2.reverse()
			
			for itemed in item2:
				group_number.append(set_array_oligo[set_num].index(itemed))
			
			for oligo in item2:
				print_array = []
				unique_array = []
				for oligo_thing in oligo:
					if oligo_thing not in check_array:	
						unique_array.append(oligo_thing)
						print_array.append(oligo_thing)	
					check_array.append(oligo_thing)
				final_data_1_oligo.append((unique_array,group_number[group_num]))
				
				order_key.write(key_num,0,'Group: ' + str(group_number[group_num]) + '	' + str(len(oligo)) + ' strains' + '	' + str(len(unique_array)) + ' unique strains')
				key_num += 1
	
				for pk in print_array:
					order_key.write(key_num,0,pk)
					key_num += 1		
				key_num += 1
				index_num += 1
				group_num += 1
				thing_unique_array.append(print_array)
				
			order_key.write(key_num,0,'Reverse primer set key: ',easyxf('font: bold True;'))
			key_num += 1	
			group_num = 0
			index_num = 0
			check_array = []
			thing_unique_array = []
			print_array = []
			
			final_data_oligo.append(final_data_1_oligo)
			
			group_number = []
			item2 = []
			item2 = set_array_right[set_num][:]
			item2.sort(lambda x,y: cmp(len(x), len(y)))
			item2.reverse()
			
			for itemed in item2:
				group_number.append(set_array_right[set_num].index(itemed))
			
			for right in item2:
				print_array = []
				unique_array = []
				for right_thing in right:
					if right_thing not in check_array:	
						unique_array.append(right_thing)
						print_array.append(right_thing)
					check_array.append(right_thing)
				final_data_1_right.append((unique_array,group_number[group_num]))
				
				order_key.write(key_num,0,'Group: ' + str(group_number[group_num]) + '	' + str(len(right)) + ' strains' + '	' + str(len(unique_array)) + ' unique strains')
				key_num += 1
				
				for pk in print_array:
					order_key.write(key_num,0,pk)
					key_num += 1
				key_num += 1
				index_num += 1
				thing_unique_array.append(print_array)
				group_num += 1
			set_num += 1
				
			final_data_right.append(final_data_1_right)
	
		primer_number = 0

		for item in final_data_left:
			captured = 0
			item = sorted(item, key=lambda student: len(student[0]), reverse=True)
			orders_key.write(orders_num,0,'**********')
			orders_num += 1
			orders_key.write(orders_num,0,'Primer set: ' + str(primer_number))
			orders_num += 1
			orders_num += 1
			orders_key.write(orders_num,0,'Forward Primer: ')
			orders_num += 1

			for x in range(0,primer_set_max):
				try:
					if len(item[x][0]) > 0:
						orders_key.write(orders_num,0,'Group ' + str(item[x][1]))
						orders_num += 1
						orders_key.write(orders_num,0,'Order Sequence: ' + str(final_left_sort[primer_number][item[x][1]]))
						orders_num += 1
						orders_key.write(orders_num,0,str(len(item[x][0])) + ' unique strains captured')
						orders_num += 1
						orders_num += 1
						captured += len(item[x][0])
				except:
					a = 'a'
			percentage_captured = (Decimal(captured)/Decimal(total_seq)) * 100
			
			orders_key.write(orders_num,0,'Percentage captured:\t' + str(percentage_captured))
			orders_num += 1
			orders_num += 1
			
			oligo = final_data_oligo[primer_number]
			oligo = sorted(oligo, key=lambda student: len(student[0]), reverse=True)
			captured = 0
			#oligo
			orders_key.write(orders_num,0,'Oligo: ')
			orders_num += 1
			for x in range(0,primer_set_max):
				try:
					if len(oligo[x][0]) > 0:
						orders_key.write(orders_num,0,'Group ' + str(oligo[x][1]))
						orders_num += 1
						orders_key.write(orders_num,0,'Order Sequence: ' + str(final_oligo_sort[primer_number][oligo[x][1]]))
						orders_num += 1
						orders_key.write(orders_num,0,str(len(oligo[x][0])) + ' unique strains captured')
						orders_num += 1
						orders_num += 1
						captured += len(oligo[x][0])
				except:
					a = 'a'
			percentage_captured = (Decimal(captured)/Decimal(total_seq)) * 100
			
			orders_key.write(orders_num,0,'Percentage captured:\t' + str(percentage_captured))
			orders_num += 1
			orders_num += 1
			
			right = final_data_right[primer_number]
			right = sorted(right, key=lambda student: len(student[0]), reverse=True)
			captured = 0
			#reverse
			orders_key.write(orders_num,0,'Reverse Primer: ')
			orders_num += 1
			for x in range(0,primer_set_max):
				try:
					if len(right[x][0]) > 0:
						orders_key.write(orders_num,0,'Group ' + str(right[x][1]))
						orders_num += 1
						orders_key.write(orders_num,0,'Order Sequence: ' + str(final_right_sort[primer_number][right[x][1]]))
						orders_num += 1
						orders_key.write(orders_num,0,str(len(right[x][0])) + ' unique strains captured')
						orders_num += 1
						orders_num += 1
						captured += len(right[x][0])
				except:
					a = 'a'
			percentage_captured = (Decimal(captured)/Decimal(total_seq)) * 100
			
			orders_key.write(orders_num,0,'Percentage captured:\t' + str(percentage_captured))
			orders_num += 1
			
			primer_number += 1 
	return left_primers,right_primers,oligos,left_primer_heads,right_primer_heads,oligo_heads

#function to check for cross reactors
def cross_reaction(left_primers,right_primers,oligos,left_primer_heads,right_primer_heads,oligo_heads,cross_reactors,species,species_list,book):
	query_string = ''
	def_filter = []
	filter_out = []
	filter_out_right = []
	filter_out_left = []
	id_filter = []
	if cross_reactors:
		for item in cross_reactors:
			query_string += item 
			query_string += '[Orgn]'
			if item not in cross_reactors[-1]:
				query_string += ' OR '
	elif species_list:
		query_string = 'NOT ('
		countr = 0
		for organism in species_list:
			query_string += organism
			query_string += '[Orgn]'
			if countr == len(species_list):
				query_string += ')'
			else:
				query_string += ' OR '
			countr += 1	
		query_string = species
	else:
		query_string = 'NOT ' + species + '[Orgn]'
	primer_counter = 0
	for item in left_primers:
		d=1
		while d > 0:
		#store blast hit accession numbers/gene ids, store 
			try:
				left_results_handle = NCBIWWW.qblast("blastn", "nt", item, entrez_query=query_string)
				right_results_handle = NCBIWWW.qblast("blastn", "nt", right_primers[primer_counter], entrez_query=query_string)
				d = 0
			except IOError:
				sleep(1)
				d = 1
	
		left_results_xml = left_results_handle.read()
		right_results_xml = right_results_handle.read()
		
		left_soupy = BeautifulSoup(left_results_xml)
		left_hit_id = left_soupy.find_all("hit_id")
		left_hit_def = left_soupy.find_all("hit_def")
		left_e_score = left_soupy.find_all("hsp_evalue")
		left_hit_to = left_soupy.find_all("hsp_hit-to")
		left_hit_from = left_soupy.find_all("hsp_hit-from")
		left_qsec = left_soupy.find_all("hsp_qseq")
		left_hseq = left_soupy.find_all("hsp_hseq")
		left_len_al = left_soupy.find_all("hsp_align-len")

		right_soupy = BeautifulSoup(right_results_xml)
		right_hit_id = right_soupy.find_all("hit_id")
		right_hit_def = right_soupy.find_all("hit_def")
		right_e_score = right_soupy.find_all("hsp_evalue")
		right_hit_to = right_soupy.find_all("hsp_hit-to")
		right_hit_from = right_soupy.find_all("hsp_hit-from")
		right_qsec = right_soupy.find_all("hsp_qseq")
		right_hseq = right_soupy.find_all("hsp_hseq")
		right_len_al = right_soupy.find_all("hsp_align-len")
		hit_number = 0
		
		for item2 in left_hit_id:
			r_hit_number = 0
			for item3 in right_hit_id:
				if str(item2.text) == str(item3.text):
					#if primer hits are 5000 BP from each other or less count as cross reactor
					minus = abs(int(left_hit_from[hit_number].text)-int(right_hit_from[r_hit_number].text))
					if minus < 5000:
						left_strand = int(left_hit_to[hit_number].text) - int(left_hit_from[hit_number].text)
						right_strand = int(right_hit_to[r_hit_number].text) - int(right_hit_from[r_hit_number].text)
						if left_strand < 0:
							if right_strand > 0:
								def_filter.append(left_hit_def[hit_number].text)
								id_filter.append(left_hit_id[hit_number].text)
								filter_out_right.append(right_primer_heads[primer_counter])
								filter_out_left.append(left_primer_heads[primer_counter])
						if left_strand > 0:
							if right_strand < 0:
								def_filter.append(left_hit_def[hit_number].text)
								id_filter.append(left_hit_id[hit_number].text)
								filter_out_right.append(right_primer_heads[primer_counter])
								filter_out_left.append(left_primer_heads[primer_counter])
				r_hit_number += 1
			hit_number += 1
		primer_counter += 1
	return def_filter,filter_out_left,filter_out_right,id_filter
	
def getAttachment(attachmentFilePath):
	contentType, encoding = mimetypes.guess_type(attachmentFilePath)
	if contentType is None or encoding is not None:
		contentType = 'application/octet-stream'
	mainType, subType = contentType.split('/', 1)
	file = open(attachmentFilePath, 'rb')
	if mainType == 'text':
		attachment = MIMEText(file.read())
	elif mainType == 'message':
		attachment = email.message_from_file(file)
	elif mainType == 'image':
		attachment = MIMEImage(file.read(),_subType=subType)
	elif mainType == 'audio':
		attachment = MIMEAudio(file.read(),_subType=subType)
	else:
		attachment = MIMEBase(mainType, subType)
	attachment.set_payload(file.read())
	encode_base64(attachment)
	file.close()
	attachment.add_header('Content-Disposition', 'attachment',   filename=os.path.basename(attachmentFilePath))
	return attachment

def sendMail(subject, text, attachmentFilePath,recipient):
	gmailUser = 'omegagenomics@gmail.com'
	gmailPassword = 'cat102089'
	msg = MIMEMultipart()
	msg['From'] = gmailUser
	msg['To'] = recipient
	msg['Subject'] = subject
	msg.attach(MIMEText(text,'html'))
	msg.attach(getAttachment(attachmentFilePath))
	mailServer = smtplib.SMTP('smtp.gmail.com', 587)
	mailServer.ehlo()
	mailServer.starttls()
	mailServer.ehlo()
	mailServer.login(gmailUser, gmailPassword)
	mailServer.sendmail(gmailUser, recipient, msg.as_string())
	mailServer.close()

def main():
	parser = OptionParser()
	parser.add_option("-e", "--email", action="store", type="string", dest="em")
	parser.add_option("-s", "--species", action="store", type="string", dest="species")
	parser.add_option("-g", "--gene", action="store", type="string", dest="gene")
	parser.add_option("-n", "--maxn", action="store", type="string", dest="max_n")
	parser.add_option("-p", "--slist", action="store", type="string", dest="species_list1")
	parser.add_option("-m", "--similarity", action="store", type="string", dest="similarity")
	parser.add_option("-v", "--primermax", action="store", type="string", dest="primer_set_max")
	parser.add_option("-c", "--cross", action="store", type="string", dest="cross_reactors1")
	parser.add_option("-t", "--serotypes", action="store", type="string", dest="serotypes1")
	parser.add_option("-b", "--bacteria", action="store", type="string", dest="bacteria")
	parser.add_option("-y", "--highdivergent", action="store", type="string", dest="high_divergent")
	(options, args) = parser.parse_args()
	em = options.em
	species = options.species
	gene = options.gene
	if options.max_n:
		max_n = int(options.max_n)
	else: 
		max_n = 2
	species_list1 = options.species_list1
	if options.similarity:
		similarity = Decimal(options.similarity)
	else:
		similarity = Decimal(.90)
	if options.primer_set_max:
		primer_set_max = int(options.primer_set_max)
	else:
		primer_set_max = 10
	if options.bacteria:
		bacteria = int(options.bacteria)
	else:
		bacteria = 0
	if options.high_divergent:
		high_divergent = int(options.high_divergent) 
	else:
		high_divergent = 0
	
	cross_reactors1 = options.cross_reactors1
	serotypes1 = options.serotypes1
	species_list1 = options.species_list1
	if species_list1:
		species_list2 = []
		species_list2 = species_list1.split(',')
		species_list = []
		for item in species_list2:
			if item:
				species_list.append(item.strip())
	else:
		species_list = []
	if cross_reactors1:
		cross_reactors2 = []
		cross_reactors2 = cross_reactors1.split(',')
		cross_reactors = []
		for item in cross_reactors2:
			if item:
				cross_reactors.append(item.strip())
	else:
		cross_reactors = []
	if serotypes1:
		serotypes_int = []
		serotypes_int = serotypes1.split(',')
		serotypesf = []
		serotypes = []
		for item in serotypes_int:
			serotypesf.append(item.strip())
		for thing in serotypesf:
			temp = thing.split(' ')
			temp2 = []
			for y in temp:
				if y:
					temp2.append(y.strip())
			serotypes.append(temp2)
	else:
		serotypes = []

	#comment out in production
	# em = 'jbmimms@gmail.com'
# 	species = 'Dengue virus'
# 	gene = ''
# 	max_n = 2
#   	species_list = ['Dengue virus 1','Dengue virus 2','Dengue virus 3','Dengue virus 4']
#   	primer_set_max = 10
# 	cross_reactors = []
# 	serotypes = []
#   	bacteria = 0
#   	high_divergent = 0
  
  	Entrez.email = em
  	
  	cloud_computer = 1
  	similarity = Decimal(.90)
  	ret_allowed = 5000
 	ttt = open('/Users/jared/Desktop/TestGenomes/PrimerTime/PrimerTimeDesigns/countfile.txt', 'r')
 	for line in ttt:
 		number=line
 	number = int(number)
 	ttt.close()
 
 	numbernew = number + 1
 	tttk = open('/Users/jared/Desktop/TestGenomes/PrimerTime/PrimerTimeDesigns/countfile.txt', 'w')
 	tttk.write(str(numbernew))
 	tttk.close()
 	
 	speciesc = re.sub(' ','',species)
 	
 	genefile = '/Users/jared/Desktop/TestGenomes/PrimerTime/PrimerTimeDesigns/' + speciesc + '_' + str(numbernew) + 'genefile.fasta'
 	ins2 = speciesc + '_' + str(numbernew) + 'genefile.fasta'
 	genefile2 = '/Users/jared/Desktop/TestGenomes/PrimerTime/PrimerTimeDesigns/' + speciesc + '_' +str(numbernew) + 'genefile1.fasta'
 	output2 = '/Users/jared/Desktop/TestGenomes/PrimerTime/PrimerTimeDesigns/' + speciesc + '_' + str(numbernew) + 'genefile2.fasta'
 	alignmentfile = '/Users/jared/Desktop/TestGenomes/PrimerTime/PrimerTimeDesigns/' + speciesc + '_' + str(numbernew) + 'genefile3.fasta'
 	outs2 = speciesc + '_' + str(numbernew) + 'genefile3.fasta'
 	clustalint = '/Users/jared/Desktop/TestGenomes/PrimerTime/PrimerTimeDesigns/' + speciesc + '_' + str(numbernew) + 'genefile4.fasta'
 	clustalint2 = speciesc + '_' + str(numbernew) + 'genefile4.fasta'
 	primerfile = '/Users/jared/Desktop/TestGenomes/PrimerTime/PrimerTimeDesigns/' + speciesc + '_' + str(numbernew) + 'primerfile5.fasta'
 	primerfileout = '/Users/jared/Desktop/TestGenomes/PrimerTime/PrimerTimeDesigns/' + speciesc + '_' + str(numbernew) + 'primerfile6.fasta'
 	excelfile = '/Users/jared/Desktop/TestGenomes/PrimerTime/PrimerTimeDesigns/' + speciesc + '_' + str(numbernew) + '.xls'
 	
	#need by gene for virus as well
	
	# cross_reactors = ['Aeromonas', 'Bacillus', 'Bacteriodes', 'Enterobacter', 'Enterococcus', 'Escherichia coli', 'Klebsiella', 'Peptostreptococcus', 'Porphyromonas', 'Proteus', 'Pseudomonas', 'Salmonella', 'Shigella', 'Staphylococcus', 'Vibrio', 'Yersinia', 'Cytomegalovirus', 'Human Enterovirus A', 'Human Enterovirus B', 'Human Enterovirus C', 'Human Enterovirus D', 'Norovirus', 'Rotavirus', 'Candida', 'Homo sapiens']
	
	#excel file generation
	book = Workbook()
	
	primerorder = book.add_sheet('PrimerProbesOverview')
	primerorder.panes_frozen = True
	primerorder.remove_splits = True
	primerorder.horz_split_pos = 9
	primerorder.write(0,3,species,easyxf('font: bold True;'))
	primerorder.insert_bitmap('/Users/jared/Desktop/TestGenomes/PrimerTime/PrimerTimeDesigns/primertime.bmp', 0, 0, scale_x = 1, scale_y = 1)
	primerorder.insert_bitmap('/Users/jared/Desktop/TestGenomes/PrimerTime/PrimerTimeDesigns/key.bmp', 9, 4, scale_x = 1, scale_y = 1)

	book.save(excelfile)
	
	species_string = ''
	for item in species_list:
		if str(item) == str(species_list[-1]):
			species_string += item 
		else:
			species_string += item + ', '
		
	if high_divergent:
		sim = similarity * Decimal(100)
		primerorder.write(1,3,gene,easyxf('font: bold True;'))
		primerorder.write(2,3,'High Divergence',easyxf('font: bold True;'))
		primerorder.write(3,3,'Groups ' + str(sim)[0:5] + ' percent similar',easyxf('font: bold True;'))
		primerorder.write(4,3,'Order Primers/Probes from HighDivergentOrder tab',easyxf('font: bold True;'))
	else:
		primerorder.write(1,3,gene,easyxf('font: bold True;'))
		
	
	primerorder.write(8,3,species_string,easyxf('font: bold True;'))
	
	book.add_sheet('PrimerProbes')
	book.add_sheet('Consensus')
	sheet1 = book.get_sheet(2)
	sheet1.col(0).width = 40000
	sheet1.row(2).height_mismatch = 1
	sheet1.row(2).height = 10000

	fin_hit_id = []
	
	try:
		result_ids = craft_query(species,gene,cross_reactors,bacteria,ret_allowed,species_list,serotypes,genefile,book)
		sheet = book.get_sheet(0)
		sheet.write(0,5,str(len(result_ids)) + ' sequences')
		pre_consensus = cw_alignment(genefile,alignmentfile,clustalint,cloud_computer,book,ins2,outs2,clustalint2)
		consensus_string = degen_sequence(pre_consensus)
		sheet1.write(0,0,'Consensus sequence:',easyxf('font: bold True;'))
		sheet1.write(2,0,consensus_string,easyxf('alignment: wrap True;'))
		blast_s = clean_consensus(consensus_string)
		compare_len = len(consensus_string)
		input_handle = open(genefile, "rU")
		for record in SeqIO.parse(input_handle, "fasta"):
			fin_hit_id.append(record.description)
		input_handle.close()
		left_primers,right_primers,oligos,left_primer_heads,right_primer_heads,oligo_heads = primer_call(blast_s,consensus_string,bacteria,fin_hit_id,output2,alignmentfile,max_n,high_divergent,similarity,primer_set_max,primerfile,primerfileout,book)
		def_filter,filter_out_left,filter_out_right,id_filter = cross_reaction(left_primers,right_primers,oligos,left_primer_heads,right_primer_heads,oligo_heads,cross_reactors,species,species_list,book)
		filt_counter = 0
		all_defs = []
		all_ids = []
		cross_counter  = 0
		crosses = book.add_sheet('CrossReactors')
		if def_filter:
			crosses.write(cross_counter,0,'Cross reactors detected.',easyxf('font: bold True;'))
			cross_counter += 1
			for item in def_filter:
				if item in def_filter[filt_counter + 1:]:
					all_defs.append(def_filter[filt_counter])
					all_ids.append(id_filter[filt_counter])
				if item not in def_filter[filt_counter + 1:]:
					crosses.write(cross_counter,0,str(filter_out_left[filt_counter]))
					cross_counter += 1
					crosses.write(cross_counter,0,str(filter_out_right[filt_counter]))
					cross_counter += 1
					crosses.write(cross_counter,0,'Detected cross reaction in sequences: ')
					cross_counter += 1
					
					id_count = 0
					for ids in all_ids:
						crosses.write(cross_counter,0,str(ids)+ ' ' + str(all_defs[id_count]))
						cross_counter += 1
						id_count += 1
					crosses.write(cross_counter,0,str(id_filter[filt_counter]))
					cross_counter += 1
					crosses.write(cross_counter,0,str(item))
					cross_counter += 1
					crosses.write(cross_counter,0,'**********')
					cross_counter += 1
					del all_defs[:]
					del all_ids[:]
				filt_counter += 1
		else:
			crosses.write(cross_counter,0,'No cross reactors detected.',easyxf('font: bold True;'))
			cross_counter += 1
		
		html = '''<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        
        <!-- Facebook sharing information tags -->
        <meta property="og:title" content="*|MC:SUBJECT|*" />
        
        <title>*|MC:SUBJECT|*</title>
		<style type="text/css">
			/* Client-specific Styles */
			#outlook a{padding:0;} /* Force Outlook to provide a "view in browser" button. */
			body{width:100% !important;} .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
			body{-webkit-text-size-adjust:none;} /* Prevent Webkit platforms from changing default text sizes. */

			/* Reset Styles */
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table td{border-collapse:collapse;}
			#backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;}

			/* Template Styles */

			/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: COMMON PAGE ELEMENTS /\/\/\/\/\/\/\/\/\/\ */

			/**
			* @tab Page
			* @section background color
			* @tip Set the background color for your email. You may want to choose one that matches your company's branding.
			* @theme page
			*/
			body, #backgroundTable{
				/*@editable*/ background-color:#FAFAFA;
			}

			/**
			* @tab Page
			* @section email border
			* @tip Set the border for your email.
			*/
			#templateContainer{
				/*@editable*/ border: 1px solid #DDDDDD;
			}

			/**
			* @tab Page
			* @section heading 1
			* @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
			* @style heading 1
			*/
			h1, .h1{
				/*@editable*/ color:#202020;
				display:block;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:34px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				margin-top:0 !important;
				margin-right:0 !important;
				margin-bottom:10px !important;
				margin-left:0 !important;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Page
			* @section heading 2
			* @tip Set the styling for all second-level headings in your emails.
			* @style heading 2
			*/
			h2, .h2{
				/*@editable*/ color:#202020;
				display:block;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:30px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				margin-top:0 !important;
				margin-right:0 !important;
				margin-bottom:10px !important;
				margin-left:0 !important;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Page
			* @section heading 3
			* @tip Set the styling for all third-level headings in your emails.
			* @style heading 3
			*/
			h3, .h3{
				/*@editable*/ color:#202020;
				display:block;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:26px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				margin-top:0 !important;
				margin-right:0 !important;
				margin-bottom:10px !important;
				margin-left:0 !important;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Page
			* @section heading 4
			* @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
			* @style heading 4
			*/
			h4, .h4{
				/*@editable*/ color:#202020;
				display:block;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:22px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				margin-top:0 !important;
				margin-right:0 !important;
				margin-bottom:10px !important;
				margin-left:0 !important;
				/*@editable*/ text-align:left;
			}

			/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: PREHEADER /\/\/\/\/\/\/\/\/\/\ */

			/**
			* @tab Header
			* @section preheader style
			* @tip Set the background color for your email's preheader area.
			* @theme page
			*/
			#templatePreheader{
				/*@editable*/ background-color:#FAFAFA;
			}

			/**
			* @tab Header
			* @section preheader text
			* @tip Set the styling for your email's preheader text. Choose a size and color that is easy to read.
			*/
			.preheaderContent div{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:10px;
				/*@editable*/ line-height:100%;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Header
			* @section preheader link
			* @tip Set the styling for your email's preheader links. Choose a color that helps them stand out from your text.
			*/
			.preheaderContent div a:link, .preheaderContent div a:visited, /* Yahoo! Mail Override */ .preheaderContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: HEADER /\/\/\/\/\/\/\/\/\/\ */

			/**
			* @tab Header
			* @section header style
			* @tip Set the background color and border for your email's header area.
			* @theme header
			*/
			#templateHeader{
				/*@editable*/ background-color:#FFFFFF;
				/*@editable*/ border-bottom:0;
			}

			/**
			* @tab Header
			* @section header text
			* @tip Set the styling for your email's header text. Choose a size and color that is easy to read.
			*/
			.headerContent{
				/*@editable*/ color:#202020;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:34px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				/*@editable*/ padding:0;
				/*@editable*/ text-align:center;
				/*@editable*/ vertical-align:middle;
			}

			/**
			* @tab Header
			* @section header link
			* @tip Set the styling for your email's header links. Choose a color that helps them stand out from your text.
			*/
			.headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			#headerImage{
				height:auto;
				width:600px !important;
			}

			/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: MAIN BODY /\/\/\/\/\/\/\/\/\/\ */

			/**
			* @tab Body
			* @section body style
			* @tip Set the background color for your email's body area.
			*/
			#templateContainer, .bodyContent{
				/*@editable*/ background-color:#FFFFFF;
			}

			/**
			* @tab Body
			* @section body text
			* @tip Set the styling for your email's main content text. Choose a size and color that is easy to read.
			* @theme main
			*/
			.bodyContent div{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:14px;
				/*@editable*/ line-height:150%;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Body
			* @section body link
			* @tip Set the styling for your email's main content links. Choose a color that helps them stand out from your text.
			*/
			.bodyContent div a:link, .bodyContent div a:visited, /* Yahoo! Mail Override */ .bodyContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			.bodyContent img, .fullWidthBandContent img{
				display:inline;
				height:auto;
			}

			/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: SIDEBAR /\/\/\/\/\/\/\/\/\/\ */

			/**
			* @tab Sidebar
			* @section sidebar style
			* @tip Set the background color and border for your email's sidebar area.
			*/
			#templateSidebar{
				/*@editable*/ background-color:#FFFFFF;
			}

			/**
			* @tab Sidebar
			* @section sidebar text
			* @tip Set the styling for your email's sidebar text. Choose a size and color that is easy to read.
			*/
			.sidebarContent div{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:12px;
				/*@editable*/ line-height:150%;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Sidebar
			* @section sidebar link
			* @tip Set the styling for your email's sidebar links. Choose a color that helps them stand out from your text.
			*/
			.sidebarContent div a:link, .sidebarContent div a:visited, /* Yahoo! Mail Override */ .sidebarContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			.sidebarContent img{
				display:inline;
				height:auto;
			}

			/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: COLUMNS; LEFT, RIGHT /\/\/\/\/\/\/\/\/\/\ */

			/**
			* @tab Columns
			* @section left column text
			* @tip Set the styling for your email's left column text. Choose a size and color that is easy to read.
			*/
			.leftColumnContent{
				/*@editable*/ background-color:#FFFFFF;
			}

			/**
			* @tab Columns
			* @section left column text
			* @tip Set the styling for your email's left column text. Choose a size and color that is easy to read.
			*/
			.leftColumnContent div{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:14px;
				/*@editable*/ line-height:150%;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Columns
			* @section left column link
			* @tip Set the styling for your email's left column links. Choose a color that helps them stand out from your text.
			*/
			.leftColumnContent div a:link, .leftColumnContent div a:visited, /* Yahoo! Mail Override */ .leftColumnContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			.leftColumnContent img{
				display:inline;
				height:auto;
			}

			/**
			* @tab Columns
			* @section right column text
			* @tip Set the styling for your email's right column text. Choose a size and color that is easy to read.
			*/
			.rightColumnContent{
				/*@editable*/ background-color:#FFFFFF;
			}

			/**
			* @tab Columns
			* @section right column text
			* @tip Set the styling for your email's right column text. Choose a size and color that is easy to read.
			*/
			.rightColumnContent div{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:14px;
				/*@editable*/ line-height:150%;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Columns
			* @section right column link
			* @tip Set the styling for your email's right column links. Choose a color that helps them stand out from your text.
			*/
			.rightColumnContent div a:link, .rightColumnContent div a:visited, /* Yahoo! Mail Override */ .rightColumnContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			.rightColumnContent img{
				display:inline;
				height:auto;
			}

			/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: FOOTER /\/\/\/\/\/\/\/\/\/\ */

			/**
			* @tab Footer
			* @section footer style
			* @tip Set the background color and top border for your email's footer area.
			* @theme footer
			*/
			#templateFooter{
				/*@editable*/ background-color:#FFFFFF;
				/*@editable*/ border-top:0;
			}

			/**
			* @tab Footer
			* @section footer text
			* @tip Set the styling for your email's footer text. Choose a size and color that is easy to read.
			* @theme footer
			*/
			.footerContent div{
				/*@editable*/ color:#707070;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:12px;
				/*@editable*/ line-height:125%;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Footer
			* @section footer link
			* @tip Set the styling for your email's footer links. Choose a color that helps them stand out from your text.
			*/
			.footerContent div a:link, .footerContent div a:visited, /* Yahoo! Mail Override */ .footerContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			.footerContent img{
				display:inline;
			}

			/**
			* @tab Footer
			* @section social bar style
			* @tip Set the background color and border for your email's footer social bar.
			* @theme footer
			*/
			#social{
				/*@editable*/ background-color:#FAFAFA;
				/*@editable*/ border:0;
			}

			/**
			* @tab Footer
			* @section social bar style
			* @tip Set the background color and border for your email's footer social bar.
			*/
			#social div{
				/*@editable*/ text-align:center;
			}

			/**
			* @tab Footer
			* @section utility bar style
			* @tip Set the background color and border for your email's footer utility bar.
			* @theme footer
			*/
			#utility{
				/*@editable*/ background-color:#FFFFFF;
				/*@editable*/ border:0;
			}

			/**
			* @tab Footer
			* @section utility bar style
			* @tip Set the background color and border for your email's footer utility bar.
			*/
			#utility div{
				/*@editable*/ text-align:center;
			}

			#monkeyRewards img{
				max-width:190px;
			}
		</style>
	</head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable">
            	<tr>
                	<td align="center" valign="top">
                        <!-- // Begin Template Preheader \\ -->
                        <table border="0" cellpadding="10" cellspacing="0" width="600" id="templatePreheader">
                            <tr>
                                <td valign="top" class="preheaderContent">
                                	
                                	<!-- // Begin Module: Standard Preheader \ -->
                                    <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                    	<tr>
                                        	<td valign="top">
                                            	<div mc:edit="std_preheader_content">
                                                	 PrimerTime Success!
                                                </div>
                                            </td>
                                            <!-- *|IFNOT:ARCHIVE_PAGE|* -->
											<td valign="top" width="190">
                                            	<div mc:edit="std_preheader_links">
                                                                                        </div>
                                            </td>
											<!-- *|END:IF|* -->
                                        </tr>
                                    </table>
                                	<!-- // End Module: Standard Preheader \ -->
                                
                                </td>
                            </tr>
                        </table>
                        <!-- // End Template Preheader \\ -->
                    	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                    <!-- // Begin Template Header \\ -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">
                                        <tr>
                                            <td class="headerContent">
                                            
                                            	<!-- // Begin Module: Standard Header Image \\ -->
                                            	<img src="http://imgur.com/865d1.png" style="width:600px;" id="headerImage campaign-icon" mc:label="header_image" mc:edit="header_image" mc:allowdesigner mc:allowtext />
                                            	<!-- // End Module: Standard Header Image \\ -->
                                            
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Header \\ -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top">
                                    <!-- // Begin Template Body \\ -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">
                                    	<tr>
                                            <!-- // Begin Sidebar \\ -->
                                        	<td valign="top" width="200" id="templateSidebar">
                                            	<table border="0" cellpadding="0" cellspacing="0" width="200">
                                                	<tr>
                                                    	<td valign="top" class="sidebarContent">
                                                        
                                                            <!-- // Begin Module: Social Block with Icons \\ -->
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td valign="top" style="padding-top:20px; padding-right:20px; padding-left:20px;">
                                                                        <table border="0" cellpadding="0" cellspacing="4">
                                                                            
                                                                                                                      </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!-- // End Module: Social Block with Icons \\ -->

                                                            <!-- // Begin Module: Top Image with Content \\ -->
                                                            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                <tr mc:repeatable>
                                                                    <td valign="top">
                                                                        <img src="http://imgur.com/1nyPd.png" style="width:160px;" mc:label="image" mc:edit="tiwc200_image00" />
                                                                        <div mc:edit="tiwc200_content00">
			 	                                                            <h4 class="h4">PrimerTime</h4>
			                                                                <strong>Visit PrimerTimeDesigns for all your Primer/Probe needs.</strong> 
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!-- // End Module: Top Image with Content \\ -->
                                                        
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <!-- // End Sidebar \\ -->
                                        	<td valign="top" width="400">
                                            	<table border="0" cellpadding="0" cellspacing="0" width="400">
                                                	<tr>
                                                    	<td valign="top">
                                            				<table border="0" cellpadding="0" cellspacing="0" width="400">
                                                            	<tr>
                                                                	<td valign="top" class="bodyContent">
                                                            
                                                                        <!-- // Begin Module: Standard Content \\ -->
                                                                        <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td valign="top">
						                                                            <div mc:edit="std_content00">
						                                                                <h1 class="h1">Primer Attached</h1>
						                                                                <strong>Query Successful:</strong> See the attached excel file for primer info!
						                                                                
						                                                            </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <!-- // End Module: Standard Content \\ -->
                                                            
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                		</td>
                                                    </tr>
                                                    <tr>
                                                    	<td valign="top">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="400">
                                                                <tr>
                                                                    <td valign="top" width="180" class="leftColumnContent">
                                                                    
                                                                        <!-- // Begin Module: Top Image with Content \\ -->
                                                                        <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                            <tr mc:repeatable>
                                                                                <td valign="top">
                                                                                    <img src="http://imgur.com/LJTGl.png" style="width:160px;" mc:label="image" mc:edit="tiwc200_image01" />
                                                                                    <div mc:edit="tiwc200_content01">
						 	                                                            <h4 class="h4">Free Willy</h4>
						                                                                <strong>Free Willy services:</strong> PrimerTime by bacteria or virus strain.                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <!-- // End Module: Top Image with Content \\ -->
                                                                        
                                                                    </td>
                                                                    <td valign="top" width="180" class="rightColumnContent">
                                                                    
                                                                        <!-- // Begin Module: Top Image with Content \\ -->
                                                                        <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                            <tr mc:repeatable>
                                                                                <td valign="top">
                                                                                    <img src="http://imgur.com/t3xY0.png" style="width:160px;" mc:label="image" mc:edit="tiwc200_image02" />
                                                                                    <div mc:edit="tiwc200_content02">
 						 	                                                            <h4 class="h4">Platypus Pro</h4>
						                                                                <strong>Platypus Pro services:</strong> Conduct IP search. Store and visualize primers. Get regular updates.                                                                                     </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <!-- // End Module: Top Image with Content \\ -->
                                                                                                                                            
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>                                                
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Body \\ -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top">
                                    <!-- // Begin Template Footer \\ -->
                                	<table border="0" cellpadding="10" cellspacing="0" width="600" id="templateFooter">
                                    	<tr>
                                        	<td valign="top" class="footerContent">
                                            
                                                <!-- // Begin Module: Standard Footer \\ -->
                                                <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td colspan="2" valign="middle" id="social">
                                                            <div mc:edit="std_social">
                                                                                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" width="350">
                                                            <div mc:edit="std_footer">
																<em>Copyright &copy; 2012 Omega Genomics, All rights reserved.</em>
																<br><strong>Our mailing address is:</strong>
																<br />
																14913 Huntington Gate Drive <br>
																Poway, CA 92064
                                                            </div>
                                                        </td>
                                                                                                            </tr>
                                                    
                                                </table>
                                                <!-- // End Module: Standard Footer \\ -->
                                            
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Footer \\ -->
                                </td>
                            </tr>
                        </table>
                        <br />
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>'''
		book.save(excelfile)
		sendMail(str(species)+ ' results',html,excelfile,em)
	except:
		primerorder.write(8,1,'No Primer and Probe sets found')
		primerorder.write(9,1,'Try again with different parameters')
		book.save(excelfile)
		html = '''<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        
        <!-- Facebook sharing information tags -->
        <meta property="og:title" content="*|MC:SUBJECT|*" />
        
        <title>*|MC:SUBJECT|*</title>
		<style type="text/css">
			/* Client-specific Styles */
			#outlook a{padding:0;} /* Force Outlook to provide a "view in browser" button. */
			body{width:100% !important;} .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
			body{-webkit-text-size-adjust:none;} /* Prevent Webkit platforms from changing default text sizes. */

			/* Reset Styles */
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table td{border-collapse:collapse;}
			#backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;}

			/* Template Styles */

			/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: COMMON PAGE ELEMENTS /\/\/\/\/\/\/\/\/\/\ */

			/**
			* @tab Page
			* @section background color
			* @tip Set the background color for your email. You may want to choose one that matches your company's branding.
			* @theme page
			*/
			body, #backgroundTable{
				/*@editable*/ background-color:#FAFAFA;
			}

			/**
			* @tab Page
			* @section email border
			* @tip Set the border for your email.
			*/
			#templateContainer{
				/*@editable*/ border: 1px solid #DDDDDD;
			}

			/**
			* @tab Page
			* @section heading 1
			* @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
			* @style heading 1
			*/
			h1, .h1{
				/*@editable*/ color:#202020;
				display:block;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:34px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				margin-top:0 !important;
				margin-right:0 !important;
				margin-bottom:10px !important;
				margin-left:0 !important;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Page
			* @section heading 2
			* @tip Set the styling for all second-level headings in your emails.
			* @style heading 2
			*/
			h2, .h2{
				/*@editable*/ color:#202020;
				display:block;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:30px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				margin-top:0 !important;
				margin-right:0 !important;
				margin-bottom:10px !important;
				margin-left:0 !important;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Page
			* @section heading 3
			* @tip Set the styling for all third-level headings in your emails.
			* @style heading 3
			*/
			h3, .h3{
				/*@editable*/ color:#202020;
				display:block;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:26px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				margin-top:0 !important;
				margin-right:0 !important;
				margin-bottom:10px !important;
				margin-left:0 !important;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Page
			* @section heading 4
			* @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
			* @style heading 4
			*/
			h4, .h4{
				/*@editable*/ color:#202020;
				display:block;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:22px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				margin-top:0 !important;
				margin-right:0 !important;
				margin-bottom:10px !important;
				margin-left:0 !important;
				/*@editable*/ text-align:left;
			}

			/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: PREHEADER /\/\/\/\/\/\/\/\/\/\ */

			/**
			* @tab Header
			* @section preheader style
			* @tip Set the background color for your email's preheader area.
			* @theme page
			*/
			#templatePreheader{
				/*@editable*/ background-color:#FAFAFA;
			}

			/**
			* @tab Header
			* @section preheader text
			* @tip Set the styling for your email's preheader text. Choose a size and color that is easy to read.
			*/
			.preheaderContent div{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:10px;
				/*@editable*/ line-height:100%;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Header
			* @section preheader link
			* @tip Set the styling for your email's preheader links. Choose a color that helps them stand out from your text.
			*/
			.preheaderContent div a:link, .preheaderContent div a:visited, /* Yahoo! Mail Override */ .preheaderContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: HEADER /\/\/\/\/\/\/\/\/\/\ */

			/**
			* @tab Header
			* @section header style
			* @tip Set the background color and border for your email's header area.
			* @theme header
			*/
			#templateHeader{
				/*@editable*/ background-color:#FFFFFF;
				/*@editable*/ border-bottom:0;
			}

			/**
			* @tab Header
			* @section header text
			* @tip Set the styling for your email's header text. Choose a size and color that is easy to read.
			*/
			.headerContent{
				/*@editable*/ color:#202020;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:34px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				/*@editable*/ padding:0;
				/*@editable*/ text-align:center;
				/*@editable*/ vertical-align:middle;
			}

			/**
			* @tab Header
			* @section header link
			* @tip Set the styling for your email's header links. Choose a color that helps them stand out from your text.
			*/
			.headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			#headerImage{
				height:auto;
				width:600px !important;
			}

			/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: MAIN BODY /\/\/\/\/\/\/\/\/\/\ */

			/**
			* @tab Body
			* @section body style
			* @tip Set the background color for your email's body area.
			*/
			#templateContainer, .bodyContent{
				/*@editable*/ background-color:#FFFFFF;
			}

			/**
			* @tab Body
			* @section body text
			* @tip Set the styling for your email's main content text. Choose a size and color that is easy to read.
			* @theme main
			*/
			.bodyContent div{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:14px;
				/*@editable*/ line-height:150%;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Body
			* @section body link
			* @tip Set the styling for your email's main content links. Choose a color that helps them stand out from your text.
			*/
			.bodyContent div a:link, .bodyContent div a:visited, /* Yahoo! Mail Override */ .bodyContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			.bodyContent img, .fullWidthBandContent img{
				display:inline;
				height:auto;
			}

			/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: SIDEBAR /\/\/\/\/\/\/\/\/\/\ */

			/**
			* @tab Sidebar
			* @section sidebar style
			* @tip Set the background color and border for your email's sidebar area.
			*/
			#templateSidebar{
				/*@editable*/ background-color:#FFFFFF;
			}

			/**
			* @tab Sidebar
			* @section sidebar text
			* @tip Set the styling for your email's sidebar text. Choose a size and color that is easy to read.
			*/
			.sidebarContent div{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:12px;
				/*@editable*/ line-height:150%;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Sidebar
			* @section sidebar link
			* @tip Set the styling for your email's sidebar links. Choose a color that helps them stand out from your text.
			*/
			.sidebarContent div a:link, .sidebarContent div a:visited, /* Yahoo! Mail Override */ .sidebarContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			.sidebarContent img{
				display:inline;
				height:auto;
			}

			/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: COLUMNS; LEFT, RIGHT /\/\/\/\/\/\/\/\/\/\ */

			/**
			* @tab Columns
			* @section left column text
			* @tip Set the styling for your email's left column text. Choose a size and color that is easy to read.
			*/
			.leftColumnContent{
				/*@editable*/ background-color:#FFFFFF;
			}

			/**
			* @tab Columns
			* @section left column text
			* @tip Set the styling for your email's left column text. Choose a size and color that is easy to read.
			*/
			.leftColumnContent div{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:14px;
				/*@editable*/ line-height:150%;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Columns
			* @section left column link
			* @tip Set the styling for your email's left column links. Choose a color that helps them stand out from your text.
			*/
			.leftColumnContent div a:link, .leftColumnContent div a:visited, /* Yahoo! Mail Override */ .leftColumnContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			.leftColumnContent img{
				display:inline;
				height:auto;
			}

			/**
			* @tab Columns
			* @section right column text
			* @tip Set the styling for your email's right column text. Choose a size and color that is easy to read.
			*/
			.rightColumnContent{
				/*@editable*/ background-color:#FFFFFF;
			}

			/**
			* @tab Columns
			* @section right column text
			* @tip Set the styling for your email's right column text. Choose a size and color that is easy to read.
			*/
			.rightColumnContent div{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:14px;
				/*@editable*/ line-height:150%;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Columns
			* @section right column link
			* @tip Set the styling for your email's right column links. Choose a color that helps them stand out from your text.
			*/
			.rightColumnContent div a:link, .rightColumnContent div a:visited, /* Yahoo! Mail Override */ .rightColumnContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			.rightColumnContent img{
				display:inline;
				height:auto;
			}

			/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: FOOTER /\/\/\/\/\/\/\/\/\/\ */

			/**
			* @tab Footer
			* @section footer style
			* @tip Set the background color and top border for your email's footer area.
			* @theme footer
			*/
			#templateFooter{
				/*@editable*/ background-color:#FFFFFF;
				/*@editable*/ border-top:0;
			}

			/**
			* @tab Footer
			* @section footer text
			* @tip Set the styling for your email's footer text. Choose a size and color that is easy to read.
			* @theme footer
			*/
			.footerContent div{
				/*@editable*/ color:#707070;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:12px;
				/*@editable*/ line-height:125%;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Footer
			* @section footer link
			* @tip Set the styling for your email's footer links. Choose a color that helps them stand out from your text.
			*/
			.footerContent div a:link, .footerContent div a:visited, /* Yahoo! Mail Override */ .footerContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			.footerContent img{
				display:inline;
			}

			/**
			* @tab Footer
			* @section social bar style
			* @tip Set the background color and border for your email's footer social bar.
			* @theme footer
			*/
			#social{
				/*@editable*/ background-color:#FAFAFA;
				/*@editable*/ border:0;
			}

			/**
			* @tab Footer
			* @section social bar style
			* @tip Set the background color and border for your email's footer social bar.
			*/
			#social div{
				/*@editable*/ text-align:center;
			}

			/**
			* @tab Footer
			* @section utility bar style
			* @tip Set the background color and border for your email's footer utility bar.
			* @theme footer
			*/
			#utility{
				/*@editable*/ background-color:#FFFFFF;
				/*@editable*/ border:0;
			}

			/**
			* @tab Footer
			* @section utility bar style
			* @tip Set the background color and border for your email's footer utility bar.
			*/
			#utility div{
				/*@editable*/ text-align:center;
			}

			#monkeyRewards img{
				max-width:190px;
			}
		</style>
	</head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable">
            	<tr>
                	<td align="center" valign="top">
                        <!-- // Begin Template Preheader \\ -->
                        <table border="0" cellpadding="10" cellspacing="0" width="600" id="templatePreheader">
                            <tr>
                                <td valign="top" class="preheaderContent">
                                	
                                	<!-- // Begin Module: Standard Preheader \ -->
                                    <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                    	<tr>
                                        	<td valign="top">
                                            	<div mc:edit="std_preheader_content">
                                                	 PrimerTime Failed
                                                </div>
                                            </td>
                                            <!-- *|IFNOT:ARCHIVE_PAGE|* -->
											<td valign="top" width="190">
                                            	<div mc:edit="std_preheader_links">
                                                                                        </div>
                                            </td>
											<!-- *|END:IF|* -->
                                        </tr>
                                    </table>
                                	<!-- // End Module: Standard Preheader \ -->
                                
                                </td>
                            </tr>
                        </table>
                        <!-- // End Template Preheader \\ -->
                    	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                    <!-- // Begin Template Header \\ -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">
                                        <tr>
                                            <td class="headerContent">
                                            
                                            	<!-- // Begin Module: Standard Header Image \\ -->
                                            	<img src="http://imgur.com/865d1.png" style="width:600px;" id="headerImage campaign-icon" mc:label="header_image" mc:edit="header_image" mc:allowdesigner mc:allowtext />
                                            	<!-- // End Module: Standard Header Image \\ -->
                                            
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Header \\ -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top">
                                    <!-- // Begin Template Body \\ -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">
                                    	<tr>
                                            <!-- // Begin Sidebar \\ -->
                                        	<td valign="top" width="200" id="templateSidebar">
                                            	<table border="0" cellpadding="0" cellspacing="0" width="200">
                                                	<tr>
                                                    	<td valign="top" class="sidebarContent">
                                                        
                                                            <!-- // Begin Module: Social Block with Icons \\ -->
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td valign="top" style="padding-top:20px; padding-right:20px; padding-left:20px;">
                                                                        <table border="0" cellpadding="0" cellspacing="4">
                                                                            
                                                                                                                      </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!-- // End Module: Social Block with Icons \\ -->

                                                            <!-- // Begin Module: Top Image with Content \\ -->
                                                            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                <tr mc:repeatable>
                                                                    <td valign="top">
                                                                        <img src="http://imgur.com/1nyPd.png" style="width:160px;" mc:label="image" mc:edit="tiwc200_image00" />
                                                                        <div mc:edit="tiwc200_content00">
			 	                                                            <h4 class="h4">PrimerTime</h4>
			                                                                <strong>Visit PrimerTimeDesigns for all your Primer/Probe needs.</strong> 
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!-- // End Module: Top Image with Content \\ -->
                                                        
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <!-- // End Sidebar \\ -->
                                        	<td valign="top" width="400">
                                            	<table border="0" cellpadding="0" cellspacing="0" width="400">
                                                	<tr>
                                                    	<td valign="top">
                                            				<table border="0" cellpadding="0" cellspacing="0" width="400">
                                                            	<tr>
                                                                	<td valign="top" class="bodyContent">
                                                            
                                                                        <!-- // Begin Module: Standard Content \\ -->
                                                                        <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td valign="top">
						                                                            <div mc:edit="std_content00">
						                                                                <h1 class="h1">Primer Attached</h1>
						                                                                <strong>Query Failed:</strong> Try altering your species query (i.e. Human Adenovirus A rather than Adenovirus A) and proofing your user input. Use the High Divergent feature as a last resort. 					                                                                
						                                                            </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <!-- // End Module: Standard Content \\ -->
                                                            
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                		</td>
                                                    </tr>
                                                    <tr>
                                                    	<td valign="top">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="400">
                                                                <tr>
                                                                    <td valign="top" width="180" class="leftColumnContent">
                                                                    
                                                                        <!-- // Begin Module: Top Image with Content \\ -->
                                                                        <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                            <tr mc:repeatable>
                                                                                <td valign="top">
                                                                                    <img src="http://imgur.com/LJTGl.png" style="width:160px;" mc:label="image" mc:edit="tiwc200_image01" />
                                                                                    <div mc:edit="tiwc200_content01">
						 	                                                            <h4 class="h4">Free Willy</h4>
						                                                                <strong>Free Willy services:</strong> PrimerTime by bacteria or virus strain.                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <!-- // End Module: Top Image with Content \\ -->
                                                                        
                                                                    </td>
                                                                    <td valign="top" width="180" class="rightColumnContent">
                                                                    
                                                                        <!-- // Begin Module: Top Image with Content \\ -->
                                                                        <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                            <tr mc:repeatable>
                                                                                <td valign="top">
                                                                                    <img src="http://imgur.com/t3xY0.png" style="width:160px;" mc:label="image" mc:edit="tiwc200_image02" />
                                                                                    <div mc:edit="tiwc200_content02">
 						 	                                                            <h4 class="h4">Platypus Pro</h4>
						                                                                <strong>Platypus Pro services:</strong> Conduct IP search. Store and visualize primers. Get regular updates.                                                                                     </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <!-- // End Module: Top Image with Content \\ -->
                                                                                                                                            
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>                                                
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Body \\ -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top">
                                    <!-- // Begin Template Footer \\ -->
                                	<table border="0" cellpadding="10" cellspacing="0" width="600" id="templateFooter">
                                    	<tr>
                                        	<td valign="top" class="footerContent">
                                            
                                                <!-- // Begin Module: Standard Footer \\ -->
                                                <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td colspan="2" valign="middle" id="social">
                                                            <div mc:edit="std_social">
                                                                                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" width="350">
                                                            <div mc:edit="std_footer">
																<em>Copyright &copy; 2012 Omega Genomics, All rights reserved.</em>
																<br><strong>Our mailing address is:</strong>
																<br />
																14913 Huntington Gate Drive <br>
																Poway, CA 92064
                                                            </div>
                                                        </td>
                                                                                                            </tr>
                                                    
                                                </table>
                                                <!-- // End Module: Standard Footer \\ -->
                                            
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Footer \\ -->
                                </td>
                            </tr>
                        </table>
                        <br />
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>'''
		sendMail(str(species)+ ' query failed',html,excelfile,em)
	try:
		os.remove(genefile)
	except:
		a = 1
	try:
 		os.remove(genefile2)
 	except:
 		a = 1
 	try:
 		os.remove(output2)
 	except:
 		a = 1
 	try:
 		os.remove(alignmentfile)
 	except:
 		a = 1
 	try:
 		os.remove(clustalint)
 	except:
 		a = 1
 	try:
 		os.remove(primerfile)
 	except:
 		a = 1
 	try:
 		os.remove(primerfileout)
 	except:
 		a = 1
 	try:
 		os.remove(excelfile)
 	except:
 		a = 1
			
if __name__ == "__main__":
	main()
	
# import urllib
# import urllib2
# 
# url = 'http://www.patentlens.net/sequence/blast/blast.html'
# values = {'seqText' : 'ATGCAGCTTGCAAATGACTG'}
# 
# data = urllib.urlencode(values)
# req = urllib2.Request(url, data)
# response = urllib2.urlopen(req)
# the_page = response.read()



